# FMEA

## Introduction
This repository contains the **Failure modes and effects analysis (FMEA)** of the AcubeSAT nanosatellite,
as defined in the design phase. The main worksheet contains a listing of all failure modes for the
components of the satellite, including their causes, effects, severity and compensating provisions.

## Screenshot
![Screenshot of FMEA](screenshot.png)

## Supporting document
For the FMEA supporting document, please refer to the [AcubeSAT CDR](https://gitlab.com/acubesat/documentation/cdr-public/)
repository. The supporting document includes:

1. High-level description of the CubeSat and its functional and physical architecture
2. FMEA assumptions and constraints
3. Failure detection, correction and prevention methods in AcubeSAT
4. Description of the FDIR concept
5. The Critical Item List (CIL)

## Disclaimer

The AcubeSAT project is carried out with the support of the Education Office of
the [European Space Agency](https://esa.int), under the educational
[Fly Your Satellite!](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/)
programme.

This repository has been authored by university students participating in the
AcubeSAT project. The views expressed herein by the authors can in no way be
taken to reflect the official opinion, or endorsement, of the European Space
Agency.
